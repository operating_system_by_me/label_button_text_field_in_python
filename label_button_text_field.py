from tkinter import *

def show_entry_fields():
   #print("First Name: %s\nLast Name: %s" % (e1.get(), e2.get()))
   f = e1.get();
   s = e2.get();
   label4 = Label(master, fg="red", text = "First name is " + f);
   label4.grid(row=4, column=0);

master = Tk()
#root = Tk();
Label(master, text="First Name", width = 10).grid(row=0)
Label(master, text="Last Name", width = 10).grid(row=1)

e1 = Entry(master)
e2 = Entry(master)

e1.grid(row=0, column=1)
e2.grid(row=1, column=1)

Button(master, text='Quit', command=master.quit).grid(row=3, column=0, sticky=W, pady=4)
Button(master, text='Show', command=show_entry_fields).grid(row=3, column=1, sticky=W, pady=4)

master.mainloop()